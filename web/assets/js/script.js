
playing = false;
source = document.getElementById('source');
audioTag = document.getElementById('song');
audioTag.volume = 0.3;

function song(){
	audio = document.getElementById('song');
	if(playing){
		audio.pause();
	}else{
		audio.play();
	}
	changeAudioIcon(null, true);
	playing = !playing;
}

function changeAudioIcon(contrast, change){

	button = document.getElementById('song-button');
	classButton = button.className;
	var classLength = classButton.length;
	var classColor = "";
	if(contrast == null){
		for (var i = classLength - 5; i < classLength ; i++) {
			classColor += classButton[i];
		}
	}else{
		classColor = contrast;
	}

	if((playing && change) || (!playing && !change)){
		button.className = "off-"+classColor;
	}else if((!playing && change) || (playing && !change)){
		button.className = "on-"+classColor;
	}
}

// function alignDesc(){
// 	info = document.getElementsByClassName('info')
// 	for (var i = info.length - 1; i >= 0; i--) {
// 		if(info[i].clientHeight < 485){
// 			desc = info[i].getElementsByClassName('desc');
// 			// desc[0].style.gridColumn = "2 / 4";
// 			styleTag = document.getElementsByTagName('STYLE');
// 			styleTag[styleTag.length - 1].innerHTML += "@media (min-width: 680px){figure div.info div.desc{grid-column:2/4;}}";


// 			// info[i].style.height = "486px";
// 		}else{
// 			desc = info[i].getElementsByClassName('desc');
// 			desc[0].style.gridColumn = "1 / 4";
// 		}
// 	}
// 	// changeMood();
// }

expand = false;

function expandMenu(changeColor){
	button = document.getElementById('expand-button');
	bannerButton = document.getElementById('banner-button');
	ul = document.getElementsByTagName('UL');
	aBg = ul[0].getElementsByTagName('A');
	//console.log(contrast);

	var oldColor = button.className;
	var newColor = "";
	//console.log(oldColor[12]);
	if(oldColor[12] == 'b'){
		oldColor = "black";
		newColor = "white";
	}else{
		oldColor = "white";
		newColor = "black";
	}

	styleTag = document.getElementsByTagName('STYLE');

	if(changeColor == null){
		if(ul[0].clientHeight == 0){
			styleTag[styleTag.length - 1].innerHTML += "@media (max-width: 500px){";
			styleTag[styleTag.length - 1].innerHTML += "menu.main ul{max-height: 185px;}";
			styleTag[styleTag.length - 1].innerHTML += "div#grid menu.main{height: 185px;max-height: 185px;max-width: 100%;}";
			styleTag[styleTag.length - 1].innerHTML += "}";

		}else{
			styleTag[styleTag.length - 1].innerHTML += "@media (max-width: 500px){";
			styleTag[styleTag.length - 1].innerHTML += "menu.main ul{max-height: 0px;}";
			styleTag[styleTag.length - 1].innerHTML += "div#grid menu.main{height: 185px;max-height: 40px;max-width: 40px;}";
			styleTag[styleTag.length - 1].innerHTML += "}";
		}

		contrast = oldColor;
		expand = !expand;

	}else{
		if(changeColor == "#F0F0F0"){
			contrast = "white";
		}else if(changeColor == "#242424"){
			contrast = "black";
		}
	}
	//console.log(changeColor);
	//console.log(contrast);
	bannerButton.className= "expand back-"+contrast;

	if(expand){
		button.className = "expand back-"+contrast;
	}
	else{
		button.className = "expand menu-"+contrast;
	}	
	
}

	last_num  = 2;
	last_num2  = last_num - 1;

function changeMood(){
	num = Math.round(Math.random() * 2 + 1);
	while(num == last_num || num == last_num2){
		num = Math.round(Math.random() * 2 + 1);
	}

	var color;
	black = "#242424";
	white = "#F0F0F0";

	if(num == 1){
		color = "#2F302C";
		contrast = white;
		shadow = 110;
		highcolor = "#606f5a";
	}

	else if(num == 2){
		color = "#FFC5B2";
		contrast = black;
	 	shadow = 110;
	 	highcolor = "rgb(142,183,164)";
	}

	else if(num == 3){
		color = "#fed5b5";
		contrast = black;
	 	shadow = 86;
	 	highcolor = "#f6564a";
	}
	/*
	else if(num == 4){
		color = "rgb(206,202,213)";
		contrast = black;
	 	shadow = 86;
	 	highcolor = "#FF99E9";
	}

	else if(num == 5){
		color = "rgb(141, 203, 180)";
		contrast = black;
	 	shadow = 86;
	 	highcolor = "rgb(129, 156, 191)";
	}
	*/

	changeAnything(color, contrast, shadow, highcolor, true);

	audio = document.getElementById('audio');
	audio.style.display = "flex";
	changeAudioIcon(contrast, false);

	bg.style.backgroundImage = "linear-gradient(#F0F0F000, "+color+" 500px)";
	
	colorOp = -1;
		
	last_num2 = last_num;
	last_num = num;

	grid = document.getElementById('grid');
	document.body.style.backgroundImage = "url('./assets/img/bg/0"+num+".jpg')";
	document.body.style.backgroundColor = color;
}


// var k = 0;
var colorOp = 0;

bg = document.getElementById('grid');

function changeColor(){
	colorOp++;
	if(colorOp%4 == 0)
		color = "#DD6D6E";//red
	else if(colorOp%4 == 1)
		color = "#F9E586";//yellow
	else if(colorOp%4 == 2)
		color = "#242424";//black
	else if(colorOp%4 == 3)
		color = "#F9E586HI";//high-yellow

	yellow = "#F9E586";
	red = "#DD6D6E";
	black = "#242424";
	white = "#F0F0F0";
	orange = "#5672D1";//"rgb(86, 114, 209)";

	if(color == yellow){
		contrast = black;
	 	shadow = 86;
	 	highcolor = red;
	 }else if(color == black){
	 	contrast = white;
	 	shadow = 86;
	 	highcolor = orange;
	 }else if(color == yellow + "HI"){
	 	contrast = black;
	 	shadow = 194;
	 	highcolor = yellow;
	 	color = white;
	 }else{
	 	contrast = black;
	 	shadow = 194;
	 	highcolor = red;
	 	color = white;
	 }
	 changeAnything(color, contrast, shadow, highcolor, false);

	audio = document.getElementById('audio');
	audio.style.display = "none";
}

function changeAnything(color, contrast, shadow, highcolor, mood){

	menu = document.getElementsByTagName("MENU");
	section = document.getElementsByTagName("SECTION");
	aside = document.getElementsByTagName("ASIDE");
	h1 = document.getElementsByTagName('H1');
	p = document.getElementsByTagName('P');
	a = document.getElementsByTagName('A');
	hr = document.getElementsByTagName('HR');
	button = document.getElementsByTagName('BUTTON');
	banner = document.getElementById('banner');
	img = document.getElementsByTagName('IMG');
	input = document.getElementsByTagName('input');
	li = document.getElementsByTagName('LI');
	logo = document.getElementsByClassName('logo');
	input = document.getElementsByTagName('INPUT');
	label = document.getElementsByTagName('LABEL');
	textarea = document.getElementsByTagName('TEXTAREA');

	highlight = document.getElementsByClassName('high-light');


	if(mood){
		bg.style.backgroundColor = "transparent";
	}else{
		bg.style.backgroundColor = color;
		bg.style.backgroundImage = "linear-gradient(#F0F0F000,#FFC89B00 500px)";
	}
	
	for (var i = menu.length - 1; i >= 0; i--) {
		menu[i].style.backgroundColor = color;
		menu[i].style.border = "3px solid "+contrast;
		menu[i].style.boxShadow = "6px 4px 0px rgb("+shadow+","+shadow+","+shadow+")";
	}

	for (var i = section.length - 1; i >= 0; i--) {
		section[i].style.backgroundColor = color;
		section[i].style.border = "3px solid "+contrast;
		section[i].style.boxShadow = "6px 4px 0px rgb("+shadow+","+shadow+","+shadow+")";
	}

	for (var i = aside.length - 1; i >= 0; i--) {
		aside[i].style.backgroundColor = color;
		aside[i].style.border = "3px solid "+contrast;
		aside[i].style.boxShadow = "6px 4px 0px rgb("+shadow+","+shadow+","+shadow+")";
	}

	for (var i = img.length - 1; i > 0; i--) {
		img[i].style.boxShadow = "6px 4px 0px rgb("+shadow+","+shadow+","+shadow+")";
	}

	for (var i = input.length - 1; i >= 0; i--) {
		input[i].style.color = contrast;
	}

	for (var i = h1.length - 1; i >= 0; i--) {
		h1[i].style.color = contrast;
	}

	for (var i = a.length - 1; i >= 0; i--) {
		a[i].style.color = contrast;
	}

	for (var i = p.length - 1; i >= 0; i--) {
		p[i].style.color = contrast;
	}

	for (var i = li.length - 1; i >= 0; i--) {
		li[i].style.color = contrast;
	}

	for (var i = button.length - 1; i >= 0; i--) {
		button[i].style.borderColor = contrast;
		button[i].style.color = contrast;
	}

	for (var i = label.length - 1; i >= 0; i--) {
		label[i].style.color = contrast;
	}

	for (var i = highlight.length - 1; i >= 0; i--) {
		highlight[i].style.backgroundColor = highcolor;
	}


	if(color == white || mood){
		color = highcolor;
	}

	hr[0].style.backgroundColor = contrast;
	banner.style.boxShadow = "6px 4px 0px rgb("+shadow+","+shadow+","+shadow+")";
	button[1].style.backgroundColor = "transparent";
	button[2].style.backgroundColor = color;
	input[input.length - 1].style.backgroundColor = highcolor;
	textarea[textarea.length - 1].style.color = contrast;

	logo[0].style.border = "dashed 6px "+contrast;

	svgLogoHigh = document.getElementsByClassName('st0');
	svgLogoText = document.getElementsByClassName('st1');

	for (var i = 0; i < svgLogoHigh.length; i++) {
		svgLogoHigh[i].style.fill = highcolor;
	}

	for (var i = 0; i < svgLogoText.length; i++) {
		svgLogoText[i].style.fill = contrast;
	}

	expandMenu(contrast);
}

var w = 0;

function expandUl(button){
	ul = document.getElementById('ul-all');
	li = ul.getElementsByTagName('LI');
	height = 0;

	for (var i = 0; i < li.length; i++) {
		// console.log(li[i].scrollHeight);
		height += li[i].clientHeight;
	}

	// console.log(li.length);
	// console.log(height);
	// console.log(10 + li[0].clientHeight + li[1].clientHeight);

	linha = "#242424";

	if(bg.style.backgroundColor == "#242424" || bg.style.backgroundColor == "rgb(36, 36, 36)"){
		linha = "F0F0F0";
	}

	if(w%2 ==0){
		ul.style.maxHeight = height + "px";
		ul.style.margin = "15px 15px";
		button.style.height = "35px";
		button.style.borderBottom = "3px solid";
		ul.style.padding = "3px 0 5px 0";
		button.style.borderBottomColor = linha;
		button.innerHTML = "All texts:";
	}else{
		ul.style.maxHeight = "0px";
		ul.style.margin = "0px 15px";
		button.style.height = "32px";
		button.style.borderBottomWidth = "0";
		ul.style.padding= "0 0 0 0";
		button.style.borderBottomColor = "transparent";
		button.innerHTML = "See more";
	}
	w++;
}

function hideBanner(){
	banner = document.getElementById('banner');
	banner.style.transition = "background 0.8s ease-in-out, box-shadow 0.4s ease-in-out, transform 0.4s cubic-bezier(.68,.03,.58,1)"
	banner.style.transform = "rotateY(90deg) perspective(7.5cm) skewY(7deg)";
	banner.style.boxShadow = "30px 4px 0 #565656";
}
